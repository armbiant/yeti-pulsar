import os

import requests
from dotenv import load_dotenv
from requests.exceptions import ConnectionError, JSONDecodeError

load_dotenv()


def send_to_sentiment():
    try:
        response = requests.post(
            url=os.environ.get("SENTIMENT_URL"), json={"content": "I reject it"}
        ).json()
        message = response.get("message")[0]
        assert message.get("label") == "NEGATIVE", "`send_to_sentiment` wrong label."
        assert message.get("score") > 0.85, "`send_to_sentiment` low score."
    except ConnectionError as err:
        return False, err
    except JSONDecodeError as err:
        return False, err
    except IndexError as err:
        return False, err
    except AssertionError as err:
        return False, err

    return True, "`send_to_sentiment` passed."


def send_to_text2int():
    try:
        response = requests.post(
            url=os.environ.get("TEXT2INT_URL"), json={"content": "forty two"}
        ).json()
        assert response.get("message") == 42, "`send_to_text2int` wrong integer."
    except ConnectionError as err:
        return False, err
    except JSONDecodeError as err:
        return False, err
    except AssertionError as err:
        return False, err

    return True, "`send_to_text2int` passed."


functions_ = [
    send_to_sentiment,
    send_to_text2int,
]


def return_results():
    responses = []
    for function_ in functions_:
        response = function_()
        responses.append(response)

    return responses


if __name__ == "__main__":
    result = return_results()
    print(*result, sep="\n")
