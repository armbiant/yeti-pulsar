"""
https://www.youtube.com/watch?v=XnSQ6sRGKzI
https://www.selenium.dev/documentation/webdriver/getting_started/install_library/
"""

import os

from dotenv import load_dotenv
from selenium import webdriver

# from selenium.webdriver.common.by import By

load_dotenv()

URL = os.environ.get("STAGING_URL")

driver = webdriver.Chrome()

driver.get(URL)

print(driver.title)
