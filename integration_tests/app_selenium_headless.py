"""
https://www.selenium.dev/documentation/webdriver/getting_started/install_library/
https://romik-kelesh.medium.com/how-to-deploy-a-python-web-scraper-with-selenium-on-heroku-1459cb3ac76c
https://www.youtube.com/watch?v=odUAJws0sPU
"""
import os

from dotenv import load_dotenv
from selenium import webdriver

# from selenium.webdriver.common.by import By


load_dotenv()

URL = os.environ.get("STAGING_URL")

options = webdriver.ChromeOptions()
options.headless = True
driver = webdriver.Chrome(options=options)
driver.get(URL)
# element = driver.find_element(By.NAME, "query")
# assert element.is_enabled()

print(driver.title)

driver.quit()
