"""
https://websocket-client.readthedocs.io/en/latest/
"""

import json
import os
import re
from json import JSONDecodeError

import requests
from dotenv import load_dotenv
from requests.exceptions import ConnectionError
from websocket import WebSocketBadStatusException
from websocket import create_connection
from pprint import pprint

"""
pulsar
heart-beat
tan-beat
"""

load_dotenv()


def check_ws_room(url, test_data_):
    try:
        response = requests.get(url)
        content = response.text
        pattern = r"wss://[a-z]+[.][a-z]+[.][a-z]{2,3}/ws/quiz/[A-Za-z0-9]+/[0-9]+"
        url = re.findall(pattern=pattern, string=content)[0]
        ws_room = url + "/theroom42/"
    except ConnectionError as err:
        return False, f"{url} {err.__class__} {err.__str__()}"
    except IndexError as err:
        return False, f"{url} {err.__class__} Could not extract the room url."
    except Exception as err:
        return False, f"{url} {err.__class__}"

    try:
        ws = create_connection(ws_room)
    except WebSocketBadStatusException as err:
        return False, f"{url} {err.__class__} {err.__str__()}"
    except Exception as err:
        return False, f"{url} {err.__class__}"
    else:
        try:
            response = ws.recv()
            assert bool(response), "Can not get a response after connection!"
            response_dict = json.loads(response)
            # pprint(response_dict)
            assert "state_name" in response_dict.keys(), "state_name not in keys!"
            assert "lang" in response_dict.keys(), "lang not in keys!"
            assert "convo_id" in response_dict.keys(), "convo id not in keys!"
        except AssertionError as err:
            ws.close()
            return False, f"{url} {err.__class__} {err.__str__()}"
        except JSONDecodeError as err:
            ws.close()
            return False, f"{url} {err.__class__} {err.__str__()}"
        except Exception as err:
            ws.close()
            return False, f"{url} {err.__class__} {err.__str__()}"
        ws.close()
        return True, f"{url} passed"


functions_ = [
    (check_ws_room, os.environ.get("MAIN_URL"), "to-pass-custom-data"),
    (check_ws_room, os.environ.get("STAGING_URL"), "to-pass-custom-data"),
]


def return_results():
    responses = []
    for function_, url_, test_data_ in functions_:
        response = function_(url_, test_data_)
        responses.append(response)

    return responses


if __name__ == "__main__":
    result = return_results()
    print(*result, sep="\n")
