import logging
import os
import sys
from logging.handlers import RotatingFileHandler
from pathlib import Path

import sentry_sdk
from dotenv import load_dotenv
from sentry_sdk.integrations.logging import LoggingIntegration

from integration_tests import pulse_qary_ws
from scripts.send_mail import send_email

load_dotenv()
PROJECT_ROOT = Path(__file__).parent.parent

logger = logging.getLogger()
logger.setLevel(os.environ.get("LOG_LEVEL"))

formatter = logging.Formatter(
    "%(name)s: %(asctime)s | %(levelname)s | %(filename)s:%(lineno)s | %(process)d >>> %(message)s"
)


stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
file_handler = RotatingFileHandler(f"{PROJECT_ROOT}/private.log", maxBytes=1024 * 10, backupCount=1)
file_handler.setFormatter(formatter)
logger.addHandler(stream_handler)
logger.addHandler(file_handler)

# All of this is already happening by default!
sentry_logging = LoggingIntegration(
    level=logging.INFO,  # Capture info and above as breadcrumbs
    event_level=logging.ERROR,  # Send errors as events
)

sentry_sdk.init(
    dsn=os.environ.get("SENTRY_DSN"),
    integrations=[
        sentry_logging,
    ],
    traces_sample_rate=1,
)

subject = "Pulsar service fail"
content = """<p>Hello,</p>
<p><b>The service has failed with the following error message:</b></p>
<p>{error_message}</p>
<p>Best regards,</p>
<p>Pulsar</p>
"""

results = []

exit_code = 0
if __name__ == "__main__":
    results.extend(pulse_qary_ws.return_results())
    logging.info("Starting a test cycle!")
    for is_passed, text in results:
        if is_passed:
            logging.info(text)
        else:
            exit_code = 1
            logging.exception(text)
            send_email(
                sender=os.environ.get('SENDGRID_EMAIL_ADDRESS'),
                receivers=os.environ.get('EMAILS_TO_SEND').split(' '),
                subject=subject,
                content=content.format(error_message=text),
            )
    sys.exit(exit_code)
