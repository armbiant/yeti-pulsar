#!/usr/bin/env bash

echo "Starting tests"
python -m pulsar.app && printf "%s\n\n" "Finished without an error" || printf "%s\n\n" "Finished with error"
